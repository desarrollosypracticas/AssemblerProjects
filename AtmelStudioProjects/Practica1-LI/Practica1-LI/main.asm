;
; Practica1-LI.asm
;
; Created: 26-feb-18 2:18:08 PM
; Author : K41.k0d3
;

;Realizar un programa que sume (0x100), (0x101), -(0x102) y 0xAB. Guarde resultado en (0x104):(0x103). 
;Simular paso a paso y observar las banderas de Z,C,N,D.
;Ensamblar a partir de la 0x200

start:
    LDS R16,0x100 ;Load direct from SRAM
	LDS R17,0x101 ;Load direct from SRAM
	LDS R18,0x102 ;
	ADD R16,R17	  ;Add two registers without carry

	STS 0x103, R16

	NEG	;Two's complements
