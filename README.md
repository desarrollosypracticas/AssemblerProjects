Ejercicios de ensamblador para sus diferentes plataformas físicas o lógicas.


![alt tag](https://sistemas.com/termino/wp-content/uploads/ensamblador.jpg)

## Lista de Ejercicios Assembler en AtmelStudio
| Nombre del Ejercicio          | Descripcion   |
| ------------------------------|---------------|
| atmel001_                     | 
| atmel002_                     | 


## Lista de Ejercicios Assembler en IAR Embedded Workbench
| Nombre del Ejercicio          | Descripcion   |
| ------------------------------|---------------|
| iar001_division64bits         | División entre dos números de 64 bits.
| iar002_multiplicacion16bits   | Multiplicación entre dos números de 16 bits.
| iar003_resta32bits   | Resta de dos números de 32 bits.
